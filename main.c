/*
   ATmega328_DS18B20.c

   Created: 12.02.2021 9:04:44
   Author : Me
*/

#include "config.h"


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


#include "../../lib/lcd1602.h"
#include "../../lib/filters.h"
#include "../../lib/onewire.h"
#include "../../lib/ds18x20/ds18x20.h"
#include "../../lib/filters.h"
#include "bme280/bme280.h"


volatile unsigned long g_counter_ms = 0;
volatile unsigned long g_lum_counting = 0;

static void show_temperature_double(double temperature) {
  char temperature_text_buffer[16] = "";

  if(temperature * 10 != 0) {
    sprintf(temperature_text_buffer, " Street: %+.1f\xDF%-2c", temperature, 'C');
  }
  else {
    sprintf(temperature_text_buffer, " Street: %.1f\xDF%-2c", temperature, 'C');
  }
  LCD_set_pos(1, 0);
  LCD_str(temperature_text_buffer);
}

int ds18_buffer[7] = {
  TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE,
};

void measure_street_temperature() {
  uint8_t sp[2];

  DS18X20_start_meas(DS18X20_POWER_EXTERN, NULL);
  _delay_ms(DS18S20_TCONV);

  ow_reset();
  DS18X20_read_scratchpad(NULL, sp, 2);
  double temperature = ((sp[1] << 8) + sp[0]) * 0.0625;
  temperature -= 0.5;

  temperature = median_filter1(temperature * 10, ds18_buffer) / 10.0;

  show_temperature_double(temperature);

}

int bme280_buffer[2][7] = {
  {TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE},
  {TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE, TEMPERATURE_NONE_VALUE},
};

void measure_bme280() {
  float temperature = bme280_readTemperature(); // in �C
  //float pressure = bme280_readPressure() / 100.0; // in mbar
  float humidity = bme280_readHumidity(); // in %

  temperature = median_filter1(temperature * 10, bme280_buffer[0]) / 10.0;
  humidity = median_filter1(humidity * 10, bme280_buffer[1]) / 10.0;

  char buffer[16] = "";

  char t_buffer[5] = "";
  char h_buffer[5] = "";

  dtostrf(temperature, 4, 1, t_buffer);
  dtostrf(humidity, 4, 1, h_buffer);
  sprintf(buffer, " +%s\xDF%c  %s%%", t_buffer, 'C', h_buffer);
  LCD_set_pos(0, 0);
  LCD_str(buffer);
}


void timer_1ms_init() {
  // ������ �� 1��, 8-bit Timer/Counter0
  TCNT0 = 0;
  // ��������� ������� 8 ��� �� 64, �� ������� 125 ���
  // ������ 125 � �������� 1 ���
  OCR0A = 124;
  // Output Compare Match A Interrupt Enable
  TIMSK0 |= (1 << OCIE0A);
  // clk T2S /64 (From prescaler)
  TCCR0B = (1 << CS01 | 1 << CS00);
}

#define OCR OCR1A

void pwr_init() {
  DDRB |= (1 << PORTB1);
  PORTB &= ~(1 << PORTB1);

  // 1 << WGM12 | 1 << WGM10 - Fast PWM, 8-bit
  // 1 << COM1A1 - Clear OC1A/OC1B on Compare Match, set OC1A/OC1B at BOTTOM (non-inverting mode)
  TCCR1A |= (1 << COM1A1 | 1 << WGM10);
  // clk T 2 S /64
  TCCR1B |= (1 << WGM12 | 1 << CS11 | 1 << CS10);
  TCNT1 = 0;
  OCR = 0xFF;
}

void adc_init() {
  // AV CC with external capacitor at AREF pin
  // MUX3..0 == 0000, ADC0
  ADMUX |= (1 << REFS0);

  ADCSRA |= (1 << ADEN | 1 << ADIE | 1 << ADPS2 | 1 << ADPS1 | 1 << ADPS0);
}

void mcu_init() {
  DS18B20_DDR |= (1 << DS18B20_DQ);
  DS18B20_PORT |= (1 << DS18B20_DQ);

  ow_set_bus(&DS18B20_PIN, &DS18B20_PORT, &DS18B20_DDR, DS18B20_DQ);

  timer_1ms_init();
  pwr_init();
  adc_init();
}

// 1ms
ISR(TIMER0_COMPA_vect) {
  TCNT0 = 0;
  g_counter_ms++;
}

ISR(ADC_vect) {
  static uint8_t lum_counter = 0;
  g_lum_counting += ADC;
  lum_counter++;

  // ������ 8 �������� ������������.

  if(lum_counter == 8) {
    unsigned long avg = g_lum_counting >> 3;

    // ���� �� ����������� ������� ����� ������ ������������ �������, �� ��������� �������.
    // � ���� ������ � ������� ��� �������� �����, �� ��������������� ������������ �������.
    if(avg < 100) {
      OCR = 100;
    }
    else if(100 == OCR) {
      OCR = 255;
    }
    g_lum_counting = 0;
    lum_counter = 0;
  }
}


int main(void) {
  unsigned long temperature_timer = 0;
  unsigned long lum_timer = 0;

  mcu_init();
  bme280_init();

  LCD_init();
  LCD_clear();

  sei();

  measure_street_temperature();
  measure_bme280();

  LCD_set_pos(0, 0);

  while(1) {

    if(0 == lum_timer) {
      lum_timer = g_counter_ms + 600;
    }
    else if(g_counter_ms >= lum_timer) {
      lum_timer = 0;
      ADCSRA |= (1 << ADSC);
    }

    if(0 == temperature_timer) {
      temperature_timer = g_counter_ms + 5000;
    }
    else if(g_counter_ms >= temperature_timer) {
      temperature_timer = 0;
      measure_street_temperature();
      measure_bme280();
    }
  }
}

